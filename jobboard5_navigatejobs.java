package TestNG;

import org.testng.annotations.Test;

import junit.framework.Assert;

import org.testng.annotations.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;

public class jobboard5_navigatejobs {
  @Test
  public void f() {
      WebDriver driver=new FirefoxDriver();
      driver.get("https://alchemy.hguy.co/jobs/");
      driver.findElement(By.cssSelector("#menu-item-24 > a:nth-child(1)")).click();
      String Title=driver.getTitle();
      System.out.println("the title of the page is :" +Title);
      Assert.assertEquals("Jobs � Alchemy Jobs", Title);
      driver.close();
      
         
  }
  @BeforeClass
  public void beforeClass() {
  }

  @AfterClass
  public void afterClass() {
  }

}
