package TestNG;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.AfterClass;

public class jobboard9_createjoblistbackend {
  @Test
  public void f() throws InterruptedException {
		WebDriver driver=new FirefoxDriver();
		driver.get("https://alchemy.hguy.co/jobs/wp-admin");
		//log in with user'root'
	    driver.findElement(By.xpath("//input[@id='user_login']")).sendKeys("root");
	    driver.findElement(By.xpath("//input[@id='user_pass']")).sendKeys("pa$$w0rd");
	    driver.findElement(By.cssSelector("input#wp-submit")).click();
	    driver.findElement(By.xpath("/html/body/div[1]/div[1]/div[2]/ul/li[7]/a/div[3]")).click();
	    driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/a")).click();
	    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	    
	    driver.findElement(By.xpath("//button[@aria-label='Close dialog']")).click();
	    
	    	   
	    driver.findElement(By.xpath("//textarea[contains(@class,'editor-post-title__input')]")).sendKeys("Developer");
	    
	    driver.findElement(By.xpath(" //*[@id='_application']")).sendKeys("sri@gmail.com");
	    driver.findElement(By.xpath("//*[@id='_company_twitter']")).sendKeys("@ibmtwitter");
	    driver.findElement(By.xpath("//*[@id='_company_twitter']")).click();
	    driver.findElement(By.xpath("//input[@type='text'][contains(@id,'expires')]")).click();
	    driver.findElement(By.xpath("//*[@id='_job_location']")).sendKeys("pune");
	    driver.findElement(By.xpath("//*[@id='_company_tagline']")).sendKeys("it is a world company");
	    driver.findElement(By.xpath("//input[contains(@placeholder,'URL to the company video')]")).sendKeys("www.pk.com");
	    //driver.findElement(By.xpath("//button[@type='button'][contains(.,'Publish�')]"));
	    //WebElement publish=driver.findElement(By.xpath("//button[@type='button'][contains(.,'Publish�')]"));
	    //Actions actions=new Actions(driver);
	    //actions.doubleClick(publish).build().perform();
	    WebElement element=driver.findElement(By.xpath("//button[contains(@class,'button is-primary')]"));
	    element.click();
	    driver.findElement(By.xpath("//button[contains(@class,'components-button editor-post-publish-button editor-post-publish-button__button is-primary')]")).click();
	   
	   driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[1]/div[3]")).click();
	  
	   Thread.sleep(2000);
   
      
	    
	    driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[1]/div/div[2]/button[2]")).click();
	    
	    driver.close();
  }
  @BeforeClass
  public void beforeClass() {
  }

  @AfterClass
  public void afterClass() {
  }

}
