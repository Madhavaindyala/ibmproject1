package TestNG;

import org.testng.annotations.Test;

import junit.framework.Assert;

import org.testng.annotations.BeforeClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;

public class jobboard1_gettitle {
	
  @Test
  public void f() {
	  
      WebDriver driver=new FirefoxDriver();
      driver.get("https://alchemy.hguy.co/jobs/");
      String title=driver.getTitle();
      System.out.println("the title of the page is " +title);
      Assert.assertEquals("Alchemy Jobs � Job Board Application", title);
      driver.close();
  }
  
  @BeforeClass
  public void beforeClass() {
	  
	  
  }

  @AfterClass
  public void afterClass() {
  }

}
