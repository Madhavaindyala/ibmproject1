package TestNG;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;

public class jobboard11_searchjobusingxpath {
  @Test
  public void f() {
      WebDriver driver=new FirefoxDriver();
      driver.get("https://alchemy.hguy.co/jobs/");
      //search for a job
           
      driver.findElement(By.xpath("/html/body/div/header/div/div/div/div/div[3]/div/nav/div/ul/li[1]/a")).click();
      driver.findElement(By.xpath("//input[@id='search_keywords']")).sendKeys("developer");
      driver.findElement(By.xpath("//input[@id='search_location']")).sendKeys("pune");
      driver.findElement(By.xpath("//input[@type='submit']")).click();
      
      driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
      // filter jobs for full time
      driver.findElement(By.xpath("//input[@id='job_type_freelance']")).click();
      driver.findElement(By.xpath("//input[@id='job_type_internship']")).click();
      driver.findElement(By.xpath("//input[@id='job_type_part-time']")).click();
      driver.findElement(By.xpath("//input[@id='job_type_temporary']")).click();
      driver.findElement(By.xpath("//input[@type='submit']")).click();
      
      driver.findElement(By.xpath("/html/body/div/div/div/div/main/article/div/div/ul/li/a/div[1]/h3")).click();
      driver.findElement(By.xpath("//h1[@class='entry-title']")).getText();
      String jobtitle=driver.findElement(By.xpath("//h1[@class='entry-title']")).getText();
      System.out.println("the title of the joblisting is :" +jobtitle);
      driver.findElement(By.xpath("//input[@class='application_button button']")).click();
      driver.close();
      
      
  }
  @BeforeClass
  public void beforeClass() {
  }

  @AfterClass
  public void afterClass() {
  }

}
