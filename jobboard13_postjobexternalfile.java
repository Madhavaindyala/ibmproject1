package TestNG;

import java.io.FileNotFoundException;
import java.io.FileReader;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.opencsv.CSVReader;

public class jobboard13_postjobexternalfile {
	
	WebDriver driver;
	WebDriverWait wait;
	
	
	  
  @Test
 
  public void postJobExportCSV() throws FileNotFoundException, Exception {
		//Navigate to post a job
		 // Reporter.log("Navigating to Post a Job");
		  driver.findElement(By.linkText("Post a Job")).click();
		  
		  //sign in
		  driver.findElement(By.cssSelector("a.button")).click();
		    driver.findElement(By.xpath("//input[@id='user_login']")).sendKeys("root");
		    driver.findElement(By.xpath("//input[@id='user_pass']")).sendKeys("pa$$w0rd");
		  WebElement loginSubmit = driver.findElement(By.id("wp-submit"));
		  wait.until(ExpectedConditions.visibilityOf(loginSubmit));
		  loginSubmit.click();  
	 	   	 
			 
		  CSVReader reader = new CSVReader(new FileReader("C:\\Users\\MADHAVAINDYALA\\eclipse-workspace\\selenium project\\src\\Resources\\activity13.csv"));
		  String[] cell;
		  while ((cell = reader.readNext()) != null) {
			  for(int i=0;i<1;i++) {
				  String jobTitle = cell[i];
				  String location = cell[i + 1];
				  String jobType = cell[i + 2];
				  String desc = cell[i + 3];
				  String company = cell[i + 4];
				  String website = cell[i + 5];
				  String tagLine = cell[i + 6];
				  String twitter = cell[i + 7];
				  
				  //Job details input
				  driver.findElement(By.id("job_title")).sendKeys(jobTitle);
				  driver.findElement(By.id("job_location")).sendKeys(location);
				  driver.findElement(By.id("job_type")).sendKeys(jobType);
				  driver.switchTo().frame("job_description_ifr");
				  driver.findElement(By.id("tinymce")).sendKeys(desc);
				  
				  driver.switchTo().defaultContent();
				  
				
				  WebElement compElement = driver.findElement(By.id("company_name"));
				  compElement.clear();
				  compElement.sendKeys(company);
				  
				  WebElement webElement = driver.findElement(By.id("company_website"));
				  webElement.clear();
				  webElement.sendKeys(website);
				  
				  WebElement tagElement = driver.findElement(By.id("company_tagline"));
				  tagElement.clear();
				  tagElement.sendKeys(tagLine);
				  
				  driver.findElement(By.id("company_video")).clear();
				  
				  WebElement twitterElement = driver.findElement(By.id("company_twitter"));
				  twitterElement.clear();
				  twitterElement.sendKeys(twitter);
				  
				  //Finding preview button and click
				
				  WebElement preview = driver.findElement(By.cssSelector("input.button"));
				  wait.until(ExpectedConditions.elementToBeClickable(preview));
				  preview.click();
				  
				  //Opening Preview page
				  wait.until(ExpectedConditions.elementToBeClickable(By.id("job_preview_submit_button")));
				 
				  driver.findElement(By.id("job_preview_submit_button")).click();  
				  
				 WebElement finalMessage = driver.findElement(By.xpath("//div[@class='entry-content clear']"));
				  System.out.println("The message is" + finalMessage.getText());
				  
				  reader.close();
			  }
		  }}
		  
		  
  @BeforeClass
  public void beforeClass() {
	  

		 driver = new FirefoxDriver();
		  wait = new WebDriverWait(driver,30);
		  driver.get("https://alchemy.hguy.co/jobs/");
	  
		
		 		 
	  	 
	  
  }

  @AfterClass
  public void afterClass() {
	  
	  driver.close();
  }
  }

  
