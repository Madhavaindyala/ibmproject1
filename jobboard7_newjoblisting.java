package TestNG;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import org.openqa.selenium.support.ui.Select;

public class jobboard7_newjoblisting {
  @Test
  public void f()  {
	  
      WebDriver driver=new FirefoxDriver();
      driver.get("https://alchemy.hguy.co/jobs/");
      driver.findElement(By.cssSelector("#menu-item-26 > a:nth-child(1)")).click();
      driver.findElement(By.cssSelector("#create_account_email")).sendKeys("Test707@gmail.com");
      driver.findElement(By.cssSelector("#job_title")).sendKeys("developer");
      driver.findElement(By.cssSelector("#job_location")).sendKeys("pune");
      driver.findElement(By.cssSelector("#job_type"));
      WebElement singleselect=driver.findElement(By.cssSelector("#job_type"));
      Select dropdown=new Select(singleselect);
      dropdown.selectByVisibleText("Temporary");
                
        driver.switchTo().frame(0);
      driver.findElement(By.cssSelector("#tinymce")).sendKeys("apply for ajob");
      driver.switchTo().defaultContent();
      
      driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
      driver.findElement(By.xpath("//input[@id='application']")).sendKeys("https://ksrtc.in");
                           
      driver.findElement(By.cssSelector("#company_name")).sendKeys("IBM");
                
      driver.findElement(By.cssSelector("input.button:nth-child(4)")).click();
      driver.findElement(By.xpath("//*[@id=\"job_preview_submit_button\"]")).click();
      //Visit the jobs page to verify the job posted or not
      driver.findElement(By.cssSelector("#menu-item-25 > a:nth-child(1)")).click();
      System.out.println("you posted for the job successfully");
      
      driver.close();
           
      	
  }
  @BeforeClass
  public void beforeClass() {
  }

  @AfterClass
  public void afterClass() {
  }

}
