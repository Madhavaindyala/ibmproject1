package TestNG;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;

public class jobboard8_backendlogin {
  @Test
  public void f() {
	  
		WebDriver driver=new FirefoxDriver();
		driver.get("https://alchemy.hguy.co/jobs/wp-admin");
		//log in with user'root'
	    driver.findElement(By.xpath("//input[@id='user_login']")).sendKeys("root");
	    driver.findElement(By.xpath("//input[@id='user_pass']")).sendKeys("pa$$w0rd");
	    driver.findElement(By.cssSelector("input#wp-submit")).click();
	    // verify the  user name after logged in 
	    String loggedinuser=driver.findElement(By.cssSelector("span.display-name:nth-child(1)")).getText();
	    System.out.println("Logged in user name is :" +loggedinuser);
	    driver.close();
  }
  @BeforeClass
  public void beforeClass() {
  }

  @AfterClass
  public void afterClass() {
  }

}
