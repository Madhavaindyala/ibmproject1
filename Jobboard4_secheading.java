package TestNG;

import org.testng.annotations.Test;

import junit.framework.Assert;

import org.testng.annotations.BeforeClass;

import static org.testng.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;

public class Jobboard4_secheading {
  @Test
  public void f() {
      WebDriver driver=new FirefoxDriver();
      driver.get("https://alchemy.hguy.co/jobs/");
     WebElement secondhead=driver.findElement(By.xpath("/html/body/div/div/div/div/main/article/div/h2"));
     String Heading=secondhead.getText();
     System.out.println("The sesond heading of the page is:" +Heading);
     
     Assert.assertEquals("Quia quis non", Heading);
     driver.close();
     
  }
  @BeforeClass
  public void beforeClass() {
  }

  @AfterClass
  public void afterClass() {
  }

}
